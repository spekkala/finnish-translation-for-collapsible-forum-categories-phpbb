See below for English description.

# Suomennos Collapsible Forum Categories -laajennukselle

Suomenkielinen kielipaketti phpBB:n Collapsible Forum Categories -laajennukselle.

## Asennus

1. Asenna Collapsible Forum Categories -laajennus ensin.

2. Lataa laajennuksen versiota vastaava kielipaketti alla olevasta luettelosta:

	- [1.0.1 – 1.0.2](https://bitbucket.org/spekkala/finnish-translation-for-collapsible-forum-categories-phpbb/downloads/collapsible-forum-categories-fi-1.0.1.zip)
	- [1.0.0](https://bitbucket.org/spekkala/finnish-translation-for-collapsible-forum-categories-phpbb/downloads/collapsible-forum-categories-fi-1_0_0.7z)

3. Pura paketin sisältö phpBB:n `ext/phpbb/collapsiblecategories`-hakemistoon.

# Finnish Translation for Collapsible Forum Categories

A Finnish language pack for the Collapsible Forum Categories extension for phpBB.

## Installation

1. Install the Collapsible Forum Categories extension first.

2. Download the language pack matching the version of the extension from the
list below:

	- [1.0.1 – 1.0.2](https://bitbucket.org/spekkala/finnish-translation-for-collapsible-forum-categories-phpbb/downloads/collapsible-forum-categories-fi-1.0.1.zip)
	- [1.0.0](https://bitbucket.org/spekkala/finnish-translation-for-collapsible-forum-categories-phpbb/downloads/collapsible-forum-categories-fi-1_0_0.7z)

3. Extract the contents of the archive into the `ext/phpbb/collapsiblecategories`
directory under your phpBB root directory.
