# Change Log

## 1.0.1 (2017-02-26)

- Include extension version 1.0.1 in `README.md`.

## 1.0.0 (2015-10-06)

- Initial release.
